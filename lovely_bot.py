from telegram.ext import Updater, Filters, MessageHandler, CommandHandler
from random import randint
import datetime
import os
import logging


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger('LovelyBot')
logger.setLevel(logging.DEBUG)


class LovelyBot:

    def __init__(self):
        timezone = datetime.timezone(datetime.timedelta(hours=3))
        self.init_datetime = datetime.datetime.now(timezone)
        bot_token = os.environ.get('BOT_TOKEN')
        self.helen_id = os.environ.get('HELEN_ID')
        logger.info('Starting lovely bot at %s for user %s', 
              self.init_datetime.strftime('%c'), self.helen_id)
        self.updater = Updater(token=bot_token, use_context=True)
        handler = MessageHandler(Filters.text, self.handle_helen_msg)
        ping_cmd_handler = CommandHandler('ping', self.handle_ping_cmd)
        self.updater.dispatcher.add_handler(ping_cmd_handler)
        self.updater.dispatcher.add_handler(handler)
        with open('phrases.lst') as file:
            self.phrases = []
            for phrase in file:
                self.phrases.append(phrase.strip())


    def handle_helen_msg(self, update, context):
        poster_id = update.effective_user.id
        chat_id = update.effective_chat.id
        message = update.message
        if str(poster_id) == self.helen_id:
            if message.date < self.init_datetime:
                logger.debug('Old message: %s', message.text)
            else:
                logger.debug('New message: %s', message.text)
                if self.need_to_talk(update):
                    phrase = self.get_phrase()
                    logger.debug('Responding with phrase: %s', phrase)
                    context.bot.send_message(chat_id=chat_id, text=phrase)


    def handle_ping_cmd(self, update, context):
        logger.debug('Got ping command.')
        context.bot.send_message(chat_id=update.effective_chat.id, text='pong')


    def start(self):
        self.updater.start_polling()


    def get_phrase(self):
        random_index = randint(0, len(self.phrases) - 1)
        return self.phrases[random_index]


    def talk_probability(self, update):
        msg = update.message.text
        words_count = len(msg.split())
        thd = 5
        probability = min(1.0, max(0, words_count - thd) * 0.1)
        return probability


    def need_to_talk(self, update):
        probability = self.talk_probability(update)
        logger.debug("Garrulity: %s", str(probability))
        definer = randint(1, 100)
        thd = 100 * probability
        logger.debug("Threshold: %i",definer)
        return definer <= thd


if __name__ == '__main__':
    bot = LovelyBot()
    logger.info('Bot is initialized.')
    bot.start()
